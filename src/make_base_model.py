# -*- coding: utf-8 -*-
"""
@author: Jason.Roth
@title: State Water Management Engineer 
@affiliation: USDA-NRCS Minnesota
@email:jason.roth@mn.usda.gov
@repo:https://bitbucket.org/jason_roth/
Created on Tue Oct 03 15:02:36 2017

This script performs some GIS ops on a wetlands shape file containing polygons
of a wetland delineations/determinations. 

The script:
    1.) Creates a polygon mask outward specified distances in the X and Y
        directions.
    2.) Clips the elevation and soils data to the polygon
    
    2.) Buffers out the wetland polygon starting at a distance.

"""
import modflowModel as mfm
import numpy as np
import arcpy as ap
import os
import pandas as pd
import shutil as sh
import arc_functions as af

def poly_to_riv(shp, mod, grd):
    """
    make a riv file from a polygon.
    1.) intersect it with a grid to get row column
    2.) calculate area
    2.) use mod top and dep to get layer and elev
    3.) use layer to get conductivity
    4.) multiply area times cond
    5.) return riv object.
    """
    # check for river file
    if not "RIV" in mod.files.keys():
        mod.init_pkg("RIV")
    
    # intersect the shape returned with rows, cols, and area.
    shp = intersect_grid(shp, grd)
    
    ## buffer from model border
    cel_buf = 5
    
    ## search cursor to iterate over
    rows = ap.da.SearchCursor(shp, ["ROW", "COL", "DEPTH", "AREA"])
    
    rc = {}
    
    ## loop over every period
    for p in range(mod.nper):
        ## counter
        cnt = 0
        ## if there are stress periods.
        if mod.files["RIV"]['itmp'][p] > 0:
            
            ## temp container for river files
            c = []
            for r in rows:

                row=r[0]
                col=r[1]
                if row>cel_buf and col>cel_buf and \
                                row< mod.nrow-cel_buf and col< mod.ncol:
                    cnt+=1
                    
                    dep = r[2]
                    area = r[3]

                    row-=1
                    col-=1
                    
                    rc['row'] = row
                    rc['col'] = col
                      
                    riv_bot = mod.elev[0,row, col]-dep
                    mod_top = mod.elev[0,row, col]
                    mod_bot = mod.elev[-1,row, col]
    
                    if riv_bot<mod_bot:
                        thk = mod_top-mod_bot
                        print("drn bot lower than mod domain")
                        print("drain depth {0}, thk only {1}".format(dep, thk))
                        riv_bot = mod_bot+0.1
                        rc['bot']=riv_bot
                        rc['lay'] = mod.get_lyr(row, col, riv_bot)
                        rc['con'] = area*mod.vcon[rc['lay']-1]
                        rc['stg']=riv_bot
                    else:
                        rc['stg'] = riv_bot
                        rc['bot'] = riv_bot
                        rc['lay'] = mod.get_lyr(row, col, riv_bot)
                        rc['con'] = area*mod.vcon[rc['lay']-1]
                    c.append(rc.copy())
            
            mod.files["RIV"]["itmp"][p] += cnt
            cnt=0
                    
            mod.files['RIV']['cells'][p]=c[:]
        else:
            mod.files["RIV"]["itmp"][p]= -1
    return mod


def mak_ghb_bot(mod, lyr, hds, con):
    mod.init_pkg("GHB")
    ghb_cls = []
    itmp = []
    npar=[]
    bc={}
    for n in range(mod.nper):
        cls=[]
        if n==0:
            for r in range(mod.nrow):
                #print(r)
                for c in range(mod.ncol):
                    bc['lay']=lyr
                    bc['row']=r+1
                    bc['col']=c+1
                    bc['hed']=hds[r,c]
                    bc['con']=con[r,c]*mod.cellsize**2
                    cls.append(bc.copy())
            ghb_cls.append(cls)
            itmp.append(len(cls))
        else:
            itmp.append(-1)
        npar.append(0)
    mod.files["GHB"]["cells"]=ghb_cls
    mod.files["GHB"]["mxactb"]=len(ghb_cls[0])
    mod.files["GHB"]["np"]=npar
    mod.files["GHB"]["itmp"]=itmp
    return mod
     
def row_col(extent, cs, x, y):
    """ extracts data from a grid at point x, y
        x, y are the coordinates at which to get the grid value
        grid is the data array to extract from
        grid_specs is the dictionary containing array bounds
    """
    cs = mod.cellsize
    xmin = extent[0]
    xmax = extent[1]
    ymin = extent[2]
    ymax = extent[3]
    
    if y >= ymin and y <= ymax and x >=xmin and x <= xmax: ## check to see if point is in the area of the grid
        ## calculate the cell location that the point falls into within the grid
        row = int((ymax- y)) / cs + 1 ## calculate row, note not offset by 1 bc np zero indexing
        col = int((x - xmin)) / cs + 1## calculate row, note not offset by 1 bc np zero indexing
        #print row, col
        return row, col    
    
def polygon_to_rivfile(mod, shp):
    print('in progress')
    
def polylin_to_rivfile(mod, shp):
    print('in progress')
    
def shp_to_drn(mod, shp, drn_inf=None, cel_buf=10):
    ## we need lyr, row col, stage, cond, bot
    
    rows = ap.da.SearchCursor(shp, ["ROW", "COL", "DIAM", "DEPTH", "LENGTH"])
    rc = {}
    
    for p in range(mod.nper):
        cnt = 0
        
        if mod.files["RIV"]['itmp'][p] > 0:
            c = []
            for r in rows:
                
                pie = 3.14
                row=r[0]
                col=r[1]
                if row>cel_buf and col>cel_buf and \
                                row< mod.nrow-cel_buf and col< mod.ncol:
                    cnt+=1
                    dep = r[3]
                    lgt = r[4]
                    diam = r[2]
                    con=drn_inf[diam]['con']
            
                    rc['row'] = row
                    rc['col'] = col
                    row-=1
                    col-=1
                    
                    drn_bot = mod.elev[0,row, col]-dep
                    mod_top = mod.elev[0,row, col]
                    mod_bot = mod.elev[-1,row, col]
    
                    rc['con'] = con*lgt*diam*pie/12.0/3.281
                    if drn_bot<mod_bot:
                        thk = mod_top-mod_bot
                        print("drn bot lower than mod domain")
                        print("drain depth {0}, thk only {1}".format(dep, thk))
                        drn_bot = mod_bot+0.1
                        rc['bot']=drn_bot
                        rc['lay'] = mod.get_lyr(row, col, drn_bot)
                        rc['stg']=drn_bot
                    else:
                        rc['stg'] = drn_bot
                        rc['bot'] = drn_bot
                        rc['lay'] = mod.get_lyr(row, col, drn_bot)
                        
                    c.append(rc.copy())
            mod.files["RIV"]["itmp"][p]= cnt
            cnt=0
                    
            mod.files['RIV']['cells'][p]=c[:]
        else:
            mod.files["RIV"]["itmp"][p]= -1
    return mod
    
def calc_sma(arr, rng):
    """ 
    produces an sma for all points within [rng, len-rng] points falling outside
    those bounds are the average of the points located within them
    """
    rng=int(rng)
    if len(arr.shape)==1:
        sma = arr.copy()
        ub = sma.shape[0]
        sma[0:rng+1]=np.mean(arr[0:rng+1])
        sma[ub-rng:ub]=np.mean(arr[ub-rng:ub])
        for i in range(rng, ub-rng):
            sma[i]=np.mean(arr[i-rng:i+rng+1])
    else:
        sma = arr.copy()
        # go around the edges taking block averages
        
        x=0
        while x < sma.shape[1]-1:
            y=0
            while y < sma.shape[0]-1:
                ## if cell is too close to edge make accomodation
                if x >= rng:    
                    x_lb = x-rng

                elif x-rng > sma.shape[1]-1:
                    x_lb = sma.shape[1]-1
                
                else:
                    x_lb = 0
                    
                if x+rng<sma.shape[1]-1:    
                    x_ub = x+rng+1
                    
                else:
                    x_ub = sma.shape[1]  
                    
                if y >= rng:
                    
                    y_lb = y-rng
                                
                elif y-rng > sma.shape[0]-1:
                    y_lb = sma.shape[0]-1
                    
                else:
                    y_lb = 0
                    
                if y+rng<sma.shape[0]-1:
                    
                    y_ub = y+rng+1
                    
                else:
                    y_ub = sma.shape[0]-1  
                    
                #if y_ub ==  sma.shape[0] or y_lb == sma.shape[0] :
                #    print "hey Y"
                #elif x_ub == sma.shape[1] or x_lb == sma.shape[1] :
                #    print "hey X"
                #print("coord", x, y)
                #print("lower", x_lb, y_lb) 
                #print("upper", x_ub, y_ub)    
                sma[y,x]=np.mean(arr[y_lb:y_ub,x_lb:x_ub])
                y+=1
            x+=1
                
    return sma

def raster_to_ascii(in_ras, out_asc):
    chk_file(out_asc)
    ap.RasterToASCII_conversion(in_ras,out_asc)
    return out_asc


def att_table_proc(shp_path, flds):
    
    """
    produces a lookup table linking FID to another 
    string constructed from flds. These are used to look up soils in the
    soil property table.
    
    ## field containing the county fips code 
    sol_cty_fld = "AREASYMBOL"
    ## field containing the map unit symbol
    sol_mpu_fld = "MUSYM"
    
    """
    lookup = {}
    for row in ap.da.SearchCursor(shp_path, flds):
        lookup[row[0]]=row[1]+row[2]
    return lookup


def get_extent(shp_path, xoff=0, yoff=0):
    """
    get the extent of a shape buffered out a distance in the x and y direction
    """
    ext = ap.Describe(shp_path).extent
    
    bnds = [ext.XMin-xoff, ext.XMax+xoff, ext.YMin-yoff, ext.YMax+yoff]
    
    bnds = [int(b) for b in bnds]
    
    return bnds


def make_shapefile(shp_path, geo_typ="POLYGON", template =None, 
                           has_m="DISABLED", has_z="DISABLED", spa_ref=26915):
    """
    makes a shape file of a type of feature class
    """
    chk_file(shp_path)
     
    shp_path, name = os.path.split(shp_path)   
        
    spa_ref = ap.SpatialReference(spa_ref)
    
    ap.CreateFeatureclass_management(shp_path, name, geo_typ, 
                                     template, has_m, has_z, spa_ref)
    
    return os.path.join(shp_path, name)

    
def make_rectangle(shp_path, coords):
    """
    adds a rectangle to a polygon shapefile
    """
    cur = ap.da.InsertCursor(shp_path, ["SHAPE@"])
    arr = ap.Array()
    i = 0
    for yval in [2,3,3,2]:
        if i < 2:
            arr.add(ap.Point(coords[0], coords[yval]))
        else:
            arr.add(ap.Point(coords[1], coords[yval]))
        i+=1
    cur.insertRow([ap.Polygon(arr)])
    
    return shp_path
    
def get_fip_txt(cty_fip):
    """
    
    """
    if cty_fip/10.0<1.0:
        cty_fip_txt = '00{0}'.format(cty_fip)
    elif cty_fip/100.0<1.0:
        cty_fip_txt = '0{0}'.format(cty_fip)
    else:
        cty_fip_txt = '{0}'.format(cty_fip)
    return cty_fip_txt

def chk_file(f):
    if os.path.exists(f):
        ap.management.Delete(f)
        
        
def make_grid(grid_path, extent, cell_size):
    chk_file(grid_path)
    
    chk_file(grid_path.split('.')[0]+"_label."+grid_path.split('.')[1])
    
    origin_coord = "{0:0.3f} {1:0.3f}".format(extent[0],extent[2])
    
    yaxis_coord = "{0:0.3f} {1:0.3f}".format(extent[0], extent[3]+10)
    
    cs = int(cell_size)
    
    nrows = (extent[1]-extent[0])/cs
    
    ncols = (extent[3]-extent[2])/cs
    
    opposite_coord = "{0:0.3f} {1:0.3f}".format(extent[1],extent[3])
    
    ap.CreateFishnet_management(grid_path, origin_coord, yaxis_coord, cs, 
                                cs, ncols, nrows, opposite_coord, 'LABELS', 
                                '#', 'POLYGON')
    flds = ['ROW','COL']
    for f in flds:
        ap.AddField_management(grid_path, f, "SHORT")
    flds=["ROW", "COL","SHAPE@X", "SHAPE@Y"]
    with ap.da.UpdateCursor(grid_path,flds) as cursor:
        for row in cursor:  
            row[0], row[1] = row_col(extent, cs, row[2], row[3])    
            cursor.updateRow(row)
    del cursor
    
    return grid_path
 
def intersect_grid(in_shp, grd, geo_typ="line"):
    
    tmp_shp = in_shp.split('.')[0]+"_tmp."+in_shp.split('.')[1]
    
    chk_file(tmp_shp)
    
    ap.Copy_management(in_shp, tmp_shp)
    
    chk_file(in_shp)
    
    ap.Intersect_analysis([tmp_shp, grd], in_shp, "ALL")
    
    chk_file(tmp_shp)
    
    if geo_typ == "line":
    
        ap.AddField_management(in_shp, "LENGTH", "FLOAT")
        
        ap.CalculateField_management(in_shp,"LENGTH",
                                     "!shape.length@meters!","PYTHON_9.3")
    else:
        ap.AddField_management(in_shp, "AREA", "FLOAT")
        
        ap.CalculateField_management(in_shp,"AREA",
                                     "!shape.area@squaremeters!","PYTHON_9.3")        
          
    return in_shp
    
def make_drain_lines(wet_shp, ext_shp, out_shp, grid, 
                     offset=0, spacing=10, count=5, min_len=50):
    
    """
    produces buffer lines outward from the CWD. The first buffer is at a 
    distance drn_spc from the CWD. A subsequent number of buffer specified
    by drn_cnt are position at intervals of drn_spc outward from the original
    
    These lines are snipped and 
    
    """
    chk_file(out_shp)
    
    dist = []
    
    for i in range(count):
        dist.append(offset+i*spacing)
    
    temp_poly = out_shp.split('.')[0]+"_a."+out_shp.split('.')[1]

    ap.MultipleRingBuffer_analysis(wet_shp, temp_poly, dist, "meters")
    
    temp_line = out_shp.split('.')[0]+"_b."+out_shp.split('.')[1]
    
    ap.PolygonToLine_management(temp_poly, temp_line)
    
    chk_file(temp_poly)
    
    ap.Clip_analysis(temp_line, ext_shp, out_shp)

    chk_file(temp_line)
    
    rows = ap.da.UpdateCursor(out_shp, ["SHAPE@LENGTH"])
    for r in rows:
        if r[0]<min_len:
            rows.deleteRow()
            
    
    return out_shp

def clip_shape(in_shp, clp_shp, out_shp):
    
    chk_file(out_shp)

    ap.Clip_analysis(in_shp, clp_shp, out_shp)
    
    return out_shp


def clip_raster_shape(bas_ras, clp_shp, out_ras):
    chk_file(out_ras)
    
    #bas_ras = ap.Raster(bas_ras)

    bas_ras = ap.sa.ExtractByMask(bas_ras, clp_shp)
    
    bas_ras.save(out_ras)
    
    out_ras = bas_ras
    
    return out_ras
    
def clip_raster_extent(bas_ras, extent, out_ras):
    chk_file(out_ras)

    clp_str="{0} {1} {2} {3}".format(extent[0], extent[1], extent[2], extent[3])
    out_ras = ap.Clip_management(bas_ras, clp_str, out_ras)
    out_ras=ap.Raster(out_ras)

    return out_ras

def resample_raster(bas_ras, out_ras, cs=30, typ="NEAREST", snp_ras=""):
    chk_file(out_ras)
    
    if snp_ras !=  "":
        snp_pth = os.path.join(snp_ras.path,snp_ras.name)
        ap.env.snapRaster = snp_pth
    else:
        ap.env.snapRaster = None
        
    out_ras = ap.Resample_management(bas_ras, out_ras, cs, typ)
    
    out_ras = ap.Raster(out_ras)
    
    return out_ras

def proc_bdrk(bdrk_grid, out_ras, snap_rast):
    
    bdrk = ap.Raster(bdrk_grid)
    off = (bdrk.meanCellHeight+bdrk.meanCellWidth)/2
    #snap = ap.Raster(snap_rast)
    temp = os.path.split(snap_rast.path)[0]+"\\temp"
    ext = snap_rast.extent
    clp_ext=[ext.XMin-off,ext.YMin-off,ext.XMax+off,ext.YMax+off]
    bdrk = clip_raster_extent(bdrk, clp_ext, temp)
    cellsize=(snap_rast.meanCellHeight+snap_rast.meanCellWidth)/2
    temp = os.path.split(bdrk.path)[0]+"\\temp2"        
    bdrk = resample_raster(bdrk, temp, cs=cellsize, typ="NEAREST")  #, snp_ras=snap_rast)
    out_ras = ap.sa.ExtractByMask(bdrk, snap_rast)    
    return out_ras



def feature_to_raster(in_shp, in_fld, out_ras, cell_size, 
                              feat_type="POLYGON", ras_ext="", snap_ras=""):
    chk_file(out_ras)
    
    if snap_ras !=  "":
        ap.env.snapRaster = snap_ras
    else:
        ap.env.snapRaster = None
    
    if ras_ext != "":
        ap.env.extent = ap.Describe(ras_ext).extent
    else:
        ap.env.extent = None
        
    if feat_type == "POLYGON": 
        ap.PolygonToRaster_conversion(in_shp,in_fld,out_ras,cellsize=cell_size)
        
    elif feat_type == "POLYLINE":
        ap.PolylineToRaster_conversion(in_shp, in_fld, out_ras,
                                                       cellsize=cell_size)
    
    return out_ras
    

def polygon_to_polyine(in_shp, out_shp, flds={"val":{"val":1, "typ":"SHORT"}}):
    chk_file(out_shp)
    ap.PolygonToLine_management(in_shp, out_shp)
    if len(flds)>0:
        for f in flds.keys():
            ap.AddField_management(out_shp, f, flds[f]['typ'])
            ap.CalculateField_management(out_shp, f , flds[f]['val'], "PYTHON_9.3")
    return out_shp

def get_max_sol(tab_pth, sol_dic):   

    
    sol_dat = pd.read_csv(tab_pth, dtype={'num':int,'name':str,
                                       'lyr':int,'bot_dep':float,'theta_r':float,
                                       'theta_s':float,'spyd':float,'Khs':float,
                                       'Kvs':float,'Kho':float,'Kvo':float,'pct':float,
                                       'munit':str,'cty':str,'comp':str})
    
    nul_sol = []
    ## field containing the county fips code 
    #--sol_cty_fld = "AREASYMBOL"
    ## field containing the map unit symbol
    #--sol_mpu_fld = "MUSYM"
    
    ## make a cty musym column in the transect data 
    #--prof_data['ctymusym']=prof_data[sol_cty_fld]+prof_data[sol_mpu_fld].astype(str)
    
    ## make a cty musym column in the soil data 
    sol_dat['ctymusym']=sol_dat['cty']+sol_dat['munit']
    
    ## get the unique values of cnty musym in the transect data
    #--cntymusym = prof_data['ctymusym'].unique()
    
    max_soils = pd.DataFrame()
    ## for each musym in the transect get highest percentage component and get
    ## the soils data and store them for the respective layer 
     
    chk=0
    #loop over all soils in the cty map unit combo
    # retain the maximum representative soils for each mapunit on the transect
    
    done = []
    
    for k in sol_dic.keys():
        cm = sol_dic[k]
        if cm not in done:
            # get all soils in this mapunit 
            muni_soils=sol_dat[sol_dat['ctymusym']==cm]
            if muni_soils.shape[0]>0:
                # get the max soil in the mapunit
                #pcts = muni_soils['pct'].unique()
    
                max_pct = muni_soils['pct'].max()    
                
                # add the max soil to the list of soils for this transect    
                if max_soils.shape[0]==0:
                    max_soils = muni_soils[muni_soils['pct']==max_pct]
                else:
                    df = muni_soils[muni_soils['pct']==max_pct]
                    max_soils = max_soils.append(df)
            else:
                chk = 1
                if cm not in nul_sol :
                    nul_sol .append(cm)
                
            
            ## add check to make sure the HK is OK
            for c in max_soils.columns:
                for idx in max_soils.index:
                    if max_soils.loc[idx,c]<=0:
                        n = max_soils.loc[idx,'name']
                        if n not in nul_sol :
                            nul_sol .append(n)
                        chk=1
                        
            max_soils['spyd'] = max_soils['theta_s']-max_soils['theta_r']   
            done.append(cm)
    return max_soils, chk


def get_avg_sol(df):   
    """
    for ea. musym
    make a sub df
    calc props over ea comp in subdf
    del musym from orig df
    append sub df to orig df
    """    
#    sol_dat = pd.read_csv(tab_pth, dtype={'num':int,'name':str,
#                                       'lyr':int,'bot_dep':float,'theta_r':float,
#                                       'theta_s':float,'spyd':float,'Khs':float,
#                                       'Kvs':float,'Kho':float,'Kvo':float,'pct':float,
#                                       'munit':str,'cty':str,'comp':str})
    chk=0

    #done = []
    
    #nul_sol = []  
    
    sol_fld = ['theta_r','theta_s','spyd', 
                                       'Khs', 'Kvs', 'Kho', 'Kvo']

    ## dataframe to hold calculated soil props
    avg_soils = pd.DataFrame(index = df.index, columns=['gridcode', 'lyr', 'dep',
                               'theta_r','theta_s','spyd','Khs','Kvs', 'Kho', 
                               'Kvo','pct'])
    
    # for each gridcode in the soils data
    for idx in df.index:
        
        ## make a temp df of all soils for a gridcode
        musym_df = df[df.index == idx]
        
        sum_pct = musym_df['pct'].sum()
        
        # get the unique depths for this  gridcode
        lyrs = musym_df['lyr'].unique()

        ## at each depth
        for l in lyrs:
            
            ## make a separate dataframe with only these depths
            df[(df.index==df.index[0]) & (df.lyr==1)]['lyr'][6335]
            
            
            
            ## loop over the indeces
            for r in range(tdf.shape[0]):
                ## start tallying total percent
                ## if this is the first soil component to be used in calculation
                if r==0:
                    
                    ## store the initial multiplied values to the 
                    avg_soils.loc[idx][sol_fld]=tdf.iloc[r][sol_fld]*sum_pct
                else:
                    avg_soils.loc[idx][sol_fld]+=tdf.iloc[r][sol_fld]*tdf.iloc[r]['pct']
            avg_soils.loc[idx]['dep']=tdf.iloc[r]['bot_dep'] 
            avg_soils.loc[idx]['lyr']=l 
            avg_soils.loc[idx]['fid']=s       
            avg_soils.loc[idx]['ctymusym']=smu        
            avg_soils.loc[idx]['pct']=sum_pct  
            avg_soils.loc[idx][sol_fld]/=sum_pct
                   
            #done.append(smu)                 
    avg_soils['spyd'] = avg_soils['theta_s']-avg_soils['theta_r']
         
    return avg_soils, chk

def get_unique_dfvals(df, flds):
    nu_df = df[flds]
    dic = {}
    vals = nu_df[flds[0]].unique()
    for v in vals:
        dic[v]=nu_df[nu_df[flds[0]]==v][flds[1]].unique()[0]
    return dic

def make_model_arrays(mod, ext_arr, elv_arr, sol_arr, geo_arr, sol_df, geo_df, dmp_lst=[]):
    """
    mod (modflow model object) =  container for model data
    ext_ras (arcpy raster object) = the extent of the model domain 
    elv_ras (arcpy raster object) = the elev of the top model layer
    sol_ras (arcpy raster object) = zones of soils
    geo_ras (arcpy raster object) = zones of geology
    sol_df (pd dataframe) = properties and depths of soils
    geo_df (pd dataframe) = properties and depths of soils
    dmp_lst (list) = dampening factors for each layer elevation
    """
    
    print("still working this one out~~~~")
    #mod = make_elev_arrays(mod, elv_arr, sol_arr, geo_arr, sol_df, geo_df, dmp_lst)
    #mod = make_prop_arrays(mod, sol_ras, geo_ras, sol_df, geo_df)    
    return mod
    

def make_elev_arrays(mod, elv_arr, sol_arr, geo_arr, sol_df, geo_df, min_thk, dmp_lst=[]):
    """
    mod (modflow model object) =  container for model data
    ext_ras (arcpy raster object) = the extent of the model domain 
    elv_ras (arcpy raster object) = the elev of the top model layer
    sol_ras (arcpy raster object) = zones of soils
    geo_ras (arcpy raster object) = zones of geology
    sol_df (pd dataframe) = properties and depths of soils
    geo_df (pd dataframe) = properties and depths of soils
    dmp_lst (list) = dampening factors for each layer elevation
    """
    
    sol_typ = np.unique(sol_arr)
    
    geo_typ = np.unique(geo_arr)
    ## assume that all but bottom layer are assoc with soil, this is sloppy
    ## but quick for the moment
    
    
    ## nuke the old arrays:
    mod.elev=np.ones((mod.nlay+1, mod.nrow, mod.ncol))    
    for i in range(mod.nlay+1):
        
        
        ## get the dampening factor if it exists
        if len(dmp_lst) >= i:
            dmp_fac = dmp_lst[i]
        else:
            dmp_fac = 1
        
        mod.elev[i] = calc_sma(elv_arr, dmp_fac)
            
        if i > 0:
            if i < mod.nlay:
                ## now iterate through all the soils in the domain and set layer elev
                for s in sol_typ:
                    sol_idx="{0}_{1}".format(s,i)
                    mod.elev[i] = np.where(sol_arr==s,mod.elev[i]-sol_df['dep'][sol_idx]/100.0,mod.elev[i])
            else:
                for g in geo_typ:
                    ## this is a hack for floats due to binary rounding issues e.g. 1.1 ~ 1.1000000002 in binary
                    geo_idx = float("{0:0.1f}".format(g))
                    mod.elev[i] = np.where(geo_arr==g,mod.elev[i]-geo_df['dep'][geo_idx ],mod.elev[i])
                
    mod = corr_elev_arrays(mod, min_thk)
    
    return mod

def corr_elev_arrays(mod, min_thk):
    
    for i in range(mod.nlay):
        mod.elev[i+1]=np.where(mod.elev[i]-mod.elev[i+1] < 
                                min_thk, mod.elev[i]-min_thk, mod.elev[i+1])
    return mod
        

def make_prop_arrays(mod, ext_arr, sol_arr, geo_arr, sol_df, geo_df):
    
    max_sol_lay = mod.nlay-1

    sol_typ = np.unique(sol_arr)
    
    geo_typ = np.unique(geo_arr)
    
    mod.hk=np.ones((mod.nlay, mod.nrow, mod.ncol))
    mod.vk=np.ones((mod.nlay, mod.nrow, mod.ncol))
    mod.sy=np.ones((mod.nlay, mod.nrow, mod.ncol))
    
    
    for l in range(mod.nlay):
        if l < max_sol_lay:
            for s in sol_typ:
                sol_idx="{0}_{1}".format(s,l+1)
                mod.hk[l]=np.where(sol_arr==s,sol_df['Khs'][sol_idx],mod.hk[l])
                mod.vk[l]=np.where(sol_arr==s,sol_df['Kvs'][sol_idx],mod.vk[l])
                mod.sy[l]=np.where(sol_arr==s,sol_df['spyd'][sol_idx],mod.sy[l])
            mod.ss[l]=0.0001
        else:
            for g in geo_typ:
                ## this is a hack for floats due to binary rounding issues e.g. 1.1 ~ 1.1000000002 in binary
                geo_idx = float("{0:0.1f}".format(g))
                mod.hk[l]=np.where(geo_arr==g,geo_df['Kh'][geo_idx],mod.hk[l])
                mod.vk[l]=np.where(geo_arr==g,geo_df['Kv'][geo_idx],mod.vk[l])
                mod.sy[l]=np.where(geo_arr==g,geo_df['Sy'][geo_idx],mod.sy[l])
                mod.ss[l]=np.where(geo_arr==g,geo_df['Ss'][geo_idx],mod.ss[l]) 
                
    return mod


def init_uzf(mod, vks=0.1, sur_dep=0.05, opts=[], finf=0.0004, thts=0.3, thtr=0.15, 
                                                 evt_rat=0.00, evt_dep=0.3):
    mod.init_pkg('UZF')
    ##set scenario specific variables.
    mod.files['UZF']['surfdep'] = sur_dep
    mod.files['UZF']['vks']=vks
    mod.files['UZF']['thts'] = thts*np.ones((mod.nrow, mod.ncol))
    mod.files['UZF']['options']=opts
    
    mod.files['UZF']['finf'] = mod.nper*[np.ones((mod.nrow, mod.ncol))*finf]
    mod.files['UZF']['pet']=mod.nper*[np.ones((mod.nrow, mod.ncol))*evt_rat]
    mod.files['UZF']['extdp']=mod.nper*[np.ones((mod.nrow, mod.ncol))*evt_dep]
    mod.files['UZF']['extwc']=mod.nper*[np.ones((mod.nrow, mod.ncol))*((thts+thtr)/2)]   
    return mod       

    

def attr_drn_shp(shp, attr):
    for k in attr.keys():
        v = attr[k]
        ap.AddField_management(shp, k, "FLOAT")
        ap.CalculateField_management(shp, k,v, "PYTHON_9.3")

    return shp

def attr_wtld_shp(shp, fld):
    ap.AddField_management(shp, fld, "SHORT")
    v=1
    with ap.da.UpdateCursor(shp,fld) as cursor:
        for row in cursor:  
            row[0]=v
            cursor.updateRow(row)
            v+=1
    return shp 


def make_uzf_arrays(mod, ext_arr, sol_arr, sol_df):
    
    sol_typ = np.unique(sol_arr)
    vk=np.ones(( mod.nrow, mod.ncol))
    thts=np.ones((mod.nrow, mod.ncol))
    #thtr=np.ones((mod.nrow, mod.ncol))

    for s in sol_typ:
        sol_idx="{0}_{1}".format(s,1)
        vk=np.where(sol_arr==s,sol_df['Kvs'][sol_idx],vk)
        thts=np.where(sol_arr==s,sol_df['theta_s'][sol_idx],thts)
        #thtr=np.where(sol_arr==s,sol_df['theta_r'][sol_idx],thtr)
    mod.files['UZF']['vks']=vk
    mod.files['UZF']['thts'] = thts
    if mod.sim_typ == 'tr' or mod.sim_typ == 'TR':
        mod.files['UZF']['thti'] = thts
        if 'SPECIFYTHTI' not in mod.files['UZF']['options']:
            mod.files['UZF']['options'].append('SPECIFYTHTI')
    return mod
       
#def mak_drn_stg(mod, shp, attr):
#    temp = mod.files["RIV"]   
#    ip = temp["itmp"]
#    for i in ip:
#        if i == 1:
#            ap.AddField_management(shp, [k, "FLOAT", a, None, v])
            
    
##############################################################################
##############################################################################
## spa_ref (int) - ESPG code for the spatial reference of data sets, 
## default NAD83 UTM 15N = 26915

county = "Rice"

spa_ref = 26915

bas_dir = "C:\\swamp"

name = "wagner"

## model cell size either 1 or 3 m
cell_size = 3

min_thk = 0.25

soil_method = "average"

## switch to process wetlands. If the user has not made them custom
## this will take all polygons in a wetland shapefile make rasters for
## masking wetlands and make border polylines and rasters for assessing 
## impacts at wetland edge. Alternatively the user could perform these actions
## if the interpretation of wetlands in the area require manual determination.
process_wetlands = False

## polygon shapefile of wetlands
wet_shp = ""

## switch to process wetlands. If the user has not made them custom
## this will take all polygons in a wetland shapefile make rasters for
## masking wetlands and make border polylines and rasters for assessing 
## impacts at wetland edge. Alternatively the user could perform these actions
## if the interpretation of wetlands in the area require manual determination.
process_wetlands_borders = False
wbdr_shp = ""

## polygon shapefile of wetlands watersheds, not FID must correspond to those
## in wet_shp
wet_shd_shp = ""

## this is the elevation dampening list for each layer, respectively
dmp_lst = [1,3,5,10]

sim_typ = "lateral_effect" #"design_analysis", "waste_storage"
  
###############################################################################
## Pipe characteristics
###############################################################################
##http://www.ads-pipe.com/pdf/en/A1.02-Single_Wall_Perf_Patterns.pdf
##http://www.ads-pipe.com/pdf/en/A1.01_DW_HDPE_Pipe_Perforation_Patterns_01-20-15.pdf
##http://www.ads-pipe.com/pdf/en/ADH3-Hydraulics_July2014.pdf
##http://lane-enterprises.com/images/products/hdpe-pipe/brochures/hdpe%20perforation%20guide.pdf

## https://www.prinsco.com/wp-content/uploads/2014/06/ECOFLO-100-Prinsco-Dual-Wall-Specification3.pdf
##FROM LANE Ent, sent email to ADS on 11/28/2017


### MOVE THESE TO A TABLE!!!! ######################################################################
pip_dfn ={4:{"diam":4.,"len":0.875, "wid":0.120, "cnt":3, "per":0.67, "n":0.015, "dif":0.6, "con":893},
          6:{"diam":6.,"len":0.875, "wid":0.120, "cnt":3, "per":0.8, "n":0.015, "dif":1.0, "con":1755},
          8:{"diam":8.,"len":1.18, "wid":0.120, "cnt":3, "per":1.0, "n":0.016, "dif":1.5, "con":2658},
          10:{"diam":10.,"len":1.18, "wid":0.120, "cnt":3, "per":1.3, "n":0.017, "dif":2.0, "con":3041}}

## entrance loss coefficient
k_ent = 0 

## exit loss coefficient
k_ext = 0.5

  
##############################################################################
dirs = ["base_ss","base_tr"]
# begin code execution
cwd = os.path.join(bas_dir,
                       "analyses\\{0}\\{1}".format(
                       county.lower().replace(" ","_"),name))

## check out the spatial extension
ap.CheckOutExtension('Spatial')

## set the workspace
ap.env.workspace = cwd

### these are data sets created with the "clip_data.py"

grd_shp = '{0}\\gis\\grid.shp'.format(cwd)

ext_ras = '{0}\\gis\\extent'.format(cwd)

sol_ras = '{0}\\gis\\soil'.format(cwd)

sol_dat = '{0}\\dat\\soil_props.csv'.format(cwd)

geo_ras = '{0}\\gis\\surf'.format(cwd)

geo_dat = '{0}\\dat\\surf_props.csv'.format(cwd)

brk_ras = '{0}\\gis\\bdrk'.format(cwd)

brk_dat = '{0}\\dat\\bdrk_props.csv'.format(cwd)

brk_elv_ras = '{0}\\gis\\bdrk_elev'.format(cwd)

elv_ras = '{0}\\gis\\elev'.format(cwd)

wet_ras = '{0}\\gis\\wtld'.format(cwd)

wbdr_ras = '{0}\\gis\\wtld_bdr'.format(cwd)

bnds = get_extent(ext_ras)

if process_wetlands == True:
    if wet_shp == "":
        
        wet_shp = '{0}\\gis\\wtld.shp'.format(cwd)
    
    wet_ras = af.feature_to_raster(wet_shp, "FID", wet_ras, cell_size, 
                          feat_type="POLYGON", ras_ext=ext_ras, 
                              snap_ras=ext_ras)
    wet_asc = af.raster_to_ascii(wet_ras, wet_ras+'.asc')
    
    if process_wetlands_borders == True:
        
        if wbdr_shp == "":
            wbdr_shp = '{0}\\gis\\wtld_bord.shp'.format(cwd)
        else:
            wbdr_shp =af.feature_to_polyine(wet_shp, wbdr_shp)
        f = 'FID_'+wet_shp[0:7]
        wbdr_ras = af.feature_to_raster(wbdr_shp, f, wbdr_ras, 
                                           cell_size, feat_type="POLYGON", 
                                           ras_ext=ext_ras, snap_ras=ext_ras)
    

ext_ras = ap.Raster(ext_ras)

elv_ras = ap.Raster(elv_ras)

sol_ras = ap.Raster(sol_ras)

geo_ras = ap.Raster(geo_ras)

brk_ras = ap.Raster(brk_ras)

brk_elv_ras = ap.Raster(brk_elv_ras)

## dont need this
sol_dat = pd.read_csv(sol_dat, index_col='gridcode')

if soil_method == 'average':
    sol_df, chk = get_avg_sol(sol_dat)

else:
    sol_df, chk = get_max_sol(sol_dat, sol_tab)


geo_dat = pd.read_csv(geo_dat, index_col='gridcode')

brk_dat = pd.read_csv(brk_dat, index_col='gridcode')

bas_ss = "{0}\\base_ss_model\\name.nam".format(cwd)

bas_tr = "{0}\\base_tr_model\\name.nam".format(cwd)

ext_nul_val = ext_ras.noDataValue

l = sol_dat['lyr'].max()+1
r = int(elv_ras.height)
c = int(elv_ras.width)
rw = int(elv_ras.meanCellHeight)
cw = int(elv_ras.meanCellWidth)

## convert all arcpy raster objects to numpy arrays
elv_ras = ap.RasterToNumPyArray(elv_ras)
ext_ras = ap.RasterToNumPyArray(ext_ras)
sol_ras = ap.RasterToNumPyArray(sol_ras)
geo_ras = ap.RasterToNumPyArray(geo_ras)
brk_ras = ap.RasterToNumPyArray(brk_ras)
brk_elv_ras = ap.RasterToNumPyArray(brk_elv_ras)

mod = mfm.modflowModel(nam_fil=bas_ss, nlay=l, nrow=r, styp='ss',
                       ncol=c, delr=cw, delc=rw, nper=1, 
                       nstp=1, itmuni=4, lenuni=2 )

mod.nlay = l
mod.nrow = r
mod.ncol = c
mod.delr = cw
mod.delc = rw
mod.xllcorner = bnds[0]
mod.yllcorner = bnds[2]
mod.cellsize = rw

#mod.elev[0] = ap.RasterToNumPyArray(elv_ras)

mod = make_elev_arrays(mod, elv_ras, sol_ras, geo_ras, 
                                           sol_dat, geo_dat, min_thk, dmp_lst)

mod.elev[-1]=ap.RasterToNumPyArray(brk_ras)

mod = make_prop_arrays(mod, ext_ras, sol_ras, geo_ras, sol_dat, geo_dat, "max")

mod.ibnd[:] = np.where(ext_ras!=ext_nul_val,1,0)

mod.strt[:]=mod.elev[0:mod.nlay]

mod.init_pkg(pkg="UZF")

mod = init_uzf(mod, vks=mod.vk[0], sur_dep=0.05, opts=[], 
               finf=0.000404, thts=0.3, thtr=0.15, evt_rat=0, evt_dep=0.25)

mod = make_uzf_arrays(mod, ext_ras, sol_ras, sol_dat)

mod = mak_ghb_bot(mod, 3, mod.elev[-1]+1.0, mod.vk[-1]*1e-4)

grd_shp = make_grid(grd_shp, bnds, cell_size)

mod.files["NWT"]["options"]="COMPLEX"

mod.files["NWT"]['maxiterout'] = 100
#mod.files["UZF"]['options'].append("NOSURFLEAK")   
## At this point we have a working model that doesn't have any river cells.
mod.write_mod()

mod.run_mod()

hds = mod.get_heads()
mod.export_heads_ascii()
mod.strt=hds[0]
mod.strt[0]=mod.elev[0]
mod.sim_typ = "tr"

mod.str_dat[0]=[1,1,1,'tr']
mod.add_str_per(num_per=27, cpy_per=1)
mod = make_uzf_arrays(mod, ext_ras, sol_ras, sol_dat)
mod.files['UZF']['options']=["SPECIFYTHTI", "NOSURFLEAK"]
mod.files["UZF"]['finf'] = mod.nper*[np.ones((mod.nrow, mod.ncol))*0.0]
pth = os.path.join(os.path.split(mod.path)[0],"base_tr_model")
mod.change_path(pth)
mod.write_mod()
mod.run_mod()
