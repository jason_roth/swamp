# -*- coding: utf-8 -*-
"""
@author: Jason.Roth
@title: State Water Management Engineer 
@affiliation: USDA-NRCS Minnesota
@email:jason.roth@mn.usda.gov
@repo:https://bitbucket.org/jason_roth/
Created on Thu Dec 07 20:23:28 2017
"""

import modflowModel as mfm
import numpy as np
import arcpy as ap
import os
import pandas as pd

def poly_to_riv(shp, mod, grd):
    """
    make a riv file from a polygon.
    1.) intersect it with a grid to get row column
    2.) calculate area
    2.) use mod top and dep to get layer and elev
    3.) use layer to get conductivity
    4.) multiply area times cond
    5.) return riv object.
    """
    # check for river file
    if not "RIV" in mod.files.keys():
        mod.init_pkg("RIV")
    
    # intersect the shape returned with rows, cols, and area.
    shp = intersect_grid(shp, grd)
    
    ## buffer from model border
    cel_buf = 5
    
    ## search cursor to iterate over
    rows = ap.da.SearchCursor(shp, ["ROW", "COL", "DEPTH", "AREA"])
    
    rc = {}
    
    ## loop over every period
    for p in range(mod.nper):
        ## counter
        cnt = 0
        ## if there are stress periods.
        if mod.files["RIV"]['itmp'][p] > 0:
            
            ## temp container for river files
            c = []
            for r in rows:

                row=r[0]
                col=r[1]
                if row>cel_buf and col>cel_buf and \
                                row< mod.nrow-cel_buf and col< mod.ncol:
                    cnt+=1
                    
                    dep = r[2]
                    area = r[3]

                    row-=1
                    col-=1
                    
                    rc['row'] = row
                    rc['col'] = col
                      
                    riv_bot = mod.elev[0,row, col]-dep
                    mod_top = mod.elev[0,row, col]
                    mod_bot = mod.elev[-1,row, col]
    
                    if riv_bot<mod_bot:
                        thk = mod_top-mod_bot
                        print("drn bot lower than mod domain")
                        print("drain depth {0}, thk only {1}".format(dep, thk))
                        riv_bot = mod_bot+0.1
                        rc['bot']=riv_bot
                        rc['lay'] = mod.get_lyr(row, col, riv_bot)
                        rc['con'] = area*mod.vcon[rc['lay']-1]
                        rc['stg']=riv_bot
                    else:
                        rc['stg'] = riv_bot
                        rc['bot'] = riv_bot
                        rc['lay'] = mod.get_lyr(row, col, riv_bot)
                        rc['con'] = area*mod.vcon[rc['lay']-1]
                    c.append(rc.copy())
            
            mod.files["RIV"]["itmp"][p] += cnt
            cnt=0
                    
            mod.files['RIV']['cells'][p]=c[:]
        else:
            mod.files["RIV"]["itmp"][p]= -1
    return mod

def make_grid(grid_path, extent, cell_size):
    chk_file(grid_path)
    
    chk_file(grid_path.split('.')[0]+"_label."+grid_path.split('.')[1])
    
    origin_coord = "{0:0.3f} {1:0.3f}".format(extent[0],extent[2])
    
    yaxis_coord = "{0:0.3f} {1:0.3f}".format(extent[0], extent[3]+10)
    
    cs = int(cell_size)
    
    nrows = (extent[1]-extent[0])/cs
    
    ncols = (extent[3]-extent[2])/cs
    
    opposite_coord = "{0:0.3f} {1:0.3f}".format(extent[1],extent[3])
    
    ap.CreateFishnet_management(grid_path, origin_coord, yaxis_coord, cs, 
                                cs, ncols, nrows, opposite_coord, 'LABELS', 
                                '#', 'POLYGON')
    flds = ['ROW','COL']
    for f in flds:
        ap.AddField_management(grid_path, f, "SHORT")
        
    r = int(nrows)
    c = 0    
    cur = ap.UpdateCursor(grid_path)
    for row in cur:
        if c < ncols:
            c+=1
        else: 
            if r >= 1:
                c=1
                r-=1
        row.setValue('ROW', r)
        row.setValue('COL', c)       
        cur.updateRow(row)
    del cur
    
    return grid_path
 

def init_uzf(mod, vks,sur_dep, thts, thtr, evt_rat, evt_dep):
    mod.init_pkg('UZF')
    ##set scenario specific variables.
    mod.files['UZF']['surfdep'] = sur_dep
    mod.files['UZF']['vks']=vks
    mod.files['UZF']['thts'] = thts*np.ones((mod.nrow, mod.ncol))
    mod.files['UZF']['thti'] = thts*np.ones((mod.nrow, mod.ncol))
    mod.files['UZF']['pet']=mod.nper*[np.ones((mod.nrow, mod.ncol))*evt_rat]
    mod.files['UZF']['extdp']=mod.nper*[np.ones((mod.nrow, mod.ncol))*evt_dep]
    mod.files['UZF']['extwc']=mod.nper*[np.ones((mod.nrow, mod.ncol))*((thts+thtr)/2)]   
    return mod   



def calc_sma(arr, rng):
    """ 
    produces an sma for all points within [rng, len-rng] points falling outside
    those bounds are the average of the points located within them
    """
    rng=int(rng)
    if len(arr.shape)==1:
        sma = arr.copy()
        ub = sma.shape[0]
        sma[0:rng+1]=np.mean(arr[0:rng+1])
        sma[ub-rng:ub]=np.mean(arr[ub-rng:ub])
        for i in range(rng, ub-rng):
            sma[i]=np.mean(arr[i-rng:i+rng+1])
    else:
        sma = arr.copy()
        # go around the edges taking block averages
        
        x=0
        while x < sma.shape[1]-1:
            y=0
            while y < sma.shape[0]-1:
                ## if cell is too close to edge make accomodation
                if x >= rng:    
                    x_lb = x-rng

                elif x-rng > sma.shape[1]-1:
                    x_lb = sma.shape[1]-1
                
                else:
                    x_lb = 0
                    
                if x+rng<sma.shape[1]-1:    
                    x_ub = x+rng+1
                    
                else:
                    x_ub = sma.shape[1]  
                    
                if y >= rng:
                    
                    y_lb = y-rng
                                
                elif y-rng > sma.shape[0]-1:
                    y_lb = sma.shape[0]-1
                    
                else:
                    y_lb = 0
                    
                if y+rng<sma.shape[0]-1:
                    
                    y_ub = y+rng+1
                    
                else:
                    y_ub = sma.shape[0]-1  
                    
                #if y_ub ==  sma.shape[0] or y_lb == sma.shape[0] :
                #    print "hey Y"
                #elif x_ub == sma.shape[1] or x_lb == sma.shape[1] :
                #    print "hey X"
                #print("coord", x, y)
                #print("lower", x_lb, y_lb) 
                #print("upper", x_ub, y_ub)    
                sma[y,x]=np.mean(arr[y_lb:y_ub,x_lb:x_ub])
                y+=1
            x+=1
                
    return sma


def feature_to_raster(in_shp, in_fld, out_ras, cell_size, 
                              feat_type="POLYGON", ras_ext=None,snap_ras=None):
    
    chk_file(out_ras)
    
    
    if snap_ras !=  None:
        ap.env.snapRaster = snap_ras
    
    if ras_ext != None:
        ap.env.extent = ap.Describe(ras_ext).extent
    
    if feat_type == "POLYGON": 
        ap.PolygonToRaster_conversion(in_shp,in_fld,out_ras,cellsize=cell_size)
    elif feat_type == "POLYLINE":
        ap.PolylineToRaster_conversion(in_shp,in_fld,out_ras,cellsize=cell_size)
        
    out_ras = ap.Raster(out_ras)
    
    return out_ras
    
def polygon_to_polyine(in_shp, out_shp, flds={"val":{"val":1, "typ":"SHORT"}}):
    chk_file(out_shp)
    ap.PolygonToLine_management(in_shp, out_shp)
    if len(flds)>0:
        for f in flds.keys():
            ap.AddField_management(out_shp, f, flds[f]['typ'])
            ap.CalculateField_management(out_shp, f , flds[f]['val'], "PYTHON_9.3")
    return out_shp

def shp_to_riv(mod, shp, drn_inf=None, cel_buf=10):
    ## we need lyr, row col, stage, cond, bot
    mod.files['RIV']['cells']=[]
    mod.files['RIV']['itmp'][0]= 0
    rows = ap.da.SearchCursor(shp, ["ROW", "COL", "DIAM", "DEPTH", "LENGTH"])
    rc = {}
    for p in range(mod.nper):
        cnt = 0
        
        if mod.files["RIV"]['itmp'][p] >= 0:
            c = []
            for r in rows:
                
                pie = 3.14
                row=r[0]
                col=r[1]
                if row>cel_buf and col>cel_buf and \
                                row< mod.nrow-cel_buf and col< mod.ncol:
                    cnt+=1
                    dep = r[3]
                    lgt = r[4]
                    diam = r[2]
                    con=drn_inf[diam]['con']
            
                    rc['row'] = row
                    rc['col'] = col
                    row-=1
                    col-=1
                    
                    drn_bot = mod.elev[0,row, col]-dep
                    mod_top = mod.elev[0,row, col]
                    mod_bot = mod.elev[-1,row, col]
    
                    rc['con'] = con*lgt*diam*pie/12.0/3.281
                    if drn_bot<mod_bot:
                        thk = mod_top-mod_bot
                        print("drn bot lower than mod domain")
                        print("drain depth {0}, thk only {1}".format(dep, thk))
                        drn_bot = mod_bot+0.1
                        rc['bot']=drn_bot
                        rc['lay'] = mod.get_lyr(row, col, drn_bot)
                        rc['stg']=drn_bot
                    else:
                        rc['stg'] = drn_bot
                        rc['bot'] = drn_bot
                        rc['lay'] = mod.get_lyr(row, col, drn_bot)
                        
                    c.append(rc.copy())
            mod.files["RIV"]["itmp"][p]= cnt
            cnt=0
                    
            mod.files['RIV']['cells'].append(c[:])
        else:
            mod.files["RIV"]["itmp"][p]=-1
       
    mod.files["RIV"]['mxactr']=mod.get_max(mod.files['RIV']['cells'])

    return mod


def chk_file(f):
    if os.path.exists(f):
        ap.management.Delete(f)
        
def intersect_grid(in_shp, grd):
    
    tmp_shp = in_shp.split('.')[0]+"_int."+in_shp.split('.')[1]
    
    chk_file(tmp_shp)
    
    ap.Intersect_analysis([in_shp, grd], tmp_shp, "ALL")
    
    ap.AddField_management(tmp_shp, "LENGTH", "FLOAT")
    
    ap.CalculateField_management(tmp_shp, "LENGTH","!shape.length@meters!", "PYTHON_9.3")
      
    return tmp_shp        

def attr_drn_shp(shp, attr):
    for k in attr.keys():
        v = attr[k]
        ap.AddField_management(shp, k, "FLOAT")
        ap.CalculateField_management(shp, k,v, "PYTHON_9.3")

    return shp


def make_drain_lines(wet_shp, ext_shp, out_shp, grid, 
                     offset=0, spacing=10, count=5, min_len=50):
    
    """
    produces buffer lines outward from the CWD. The first buffer is at a 
    distance drn_spc from the CWD. A subsequent number of buffer specified
    by drn_cnt are position at intervals of drn_spc outward from the original
    
    These lines are snipped and 
    
    """
    chk_file(out_shp)
    
    dist = []
    
    for i in range(count):
        dist.append(offset+i*spacing)
    
    temp_poly = out_shp.split('.')[0]+"_a."+out_shp.split('.')[1]

    ap.MultipleRingBuffer_analysis(wet_shp, temp_poly, dist, "meters")
    
    temp_line = out_shp.split('.')[0]+"_b."+out_shp.split('.')[1]
    
    ap.PolygonToLine_management(temp_poly, temp_line)
    
    chk_file(temp_poly)
    
    ap.Clip_analysis(temp_line, ext_shp, out_shp)

    chk_file(temp_line)
    
    rows = ap.da.UpdateCursor(out_shp, ["SHAPE@LENGTH"])
    for r in rows:
        if r[0]<min_len:
            rows.deleteRow()
            
    
    return out_shp


cwd = os.getcwd()
## must have "DEPTH" in m and "DIAM" in inches
drn_shp = "{0}\\drains.shp".format(cwd) 

wet_shp = "{0}\\wetland.shp".format(cwd)

wet_bdr_shp = "{0}\\wetland_border.shp".format(cwd)

wet_bdr_ras = "{0}\\wet_bdr_ras".format(cwd)

ext_shp = "{0}\\bounds.shp".format(cwd)

bnd_shp = "{0}\\bounds.shp".format(cwd)

ext_ras = "{0}\\extent".format(cwd)

out_csv = "{0}\\simulation_results.csv".format(cwd)

grd_shp = "{0}\\grid.shp".format(cwd)

ref_st8 = "undrained" ##"landsurf" ## 

bas = 1

## loop over while drain offset is less than the max and while drawdown is
## within tolerance.

ddn_per = 14

## if limit this is the ddn change in subsequent simulations, else its a depth
ddn_max = 1.0/3.281 
ddn_max = 0.01

## percentage of initial 
sur_max = 0.05

ddn_dif = ddn_max+1

ddn_stat = "mean" ## "mean"

old_pth = "{0}\\base_tr_model\\name.nam".format(cwd)
new_pth = "{0}\\drain_tr_model\\".format(cwd)

pip_dfn ={4:{"diam":4.,"len":0.875, "wid":0.120, "cnt":3, "per":0.67, "n":0.015, "dif":0.6, "con":893},
          6:{"diam":6.,"len":0.875, "wid":0.120, "cnt":3, "per":0.8, "n":0.015, "dif":1.0, "con":1755},
          8:{"diam":8.,"len":1.18, "wid":0.120, "cnt":3, "per":1.0, "n":0.016, "dif":1.5, "con":2658},
          10:{"diam":10.,"len":1.18, "wid":0.120, "cnt":3, "per":1.3, "n":0.017, "dif":2.0, "con":3041}}


wet_bdr_ras = feature_to_raster(wet_bdr_shp, "val", wet_bdr_ras, 3, feat_type="POLYLINE", 
                                                ras_ext=ext_ras, snap_ras=ext_ras)

wet_bdr_ras = ap.RasterToNumPyArray(wet_bdr_ras)

wbr = np.ma.masked_where(wet_bdr_ras == 255, wet_bdr_ras)

wtlds = np.unique(wet_bdr_ras[np.where(wet_bdr_ras!=255)])


mod=mfm.modflowModel(old_pth)
mod.change_path(new_pth)
sim_res = []
#mod.files["UZF"]["options"].append("NOSURFLEAK")
mod.files["NWT"]["options"]="COMPLEX"
mod.files["NWT"]['maxiterout'] = 100
mod.write_nwt()

bas = mod.elev[0]
mod.write_uzf()
## if the reference state is the undrained state run the undrained model
## to be used in determining drawdown
if ref_st8 == "undrained":
    mod.rem_pkg("RIV")
    mod.write_nam()
    print("running base model")
    mod.run_mod()
    print("base run complete")
    h_not = mod.get_heads(0)
    bas = h_not[ddn_per-1]
    
elif ref_st8 == "landsurf":
    bas = mod.elev[0]
  
    
    
drn_shp = intersect_grid(drn_shp, grd_shp)
mod.add_pkg("RIV")
mod = shp_to_riv(mod, drn_shp, pip_dfn)


### buffer rings out consecutively 
mod.write_riv()

mod.write_nam()

mod.run_mod()


#wet_bdr_shp = polygon_to_polyine(wet_shp, wet_bdr_shp, flds={"val":{"val":1, "typ":"SHORT"}})
h_drn = mod.get_heads(0)
s_lek = mod.sum_surf_leak(1,ddn_per)
sim = h_drn[ddn_per-1]

dif = bas-sim

if ddn_stat == "mean":
    dif = np.mean(dif[np.where(wet_bdr_ras!=255)])
else:
    dif = np.max(dif[np.where(wet_bdr_ras!=255)])
    

mod.animate_simulation("cust_sim_elev",1,1,mod.nper)
mod.animate_simulation("cust_sim_depth",1,1,mod.nper, dif=True)
mod.export_heads_ascii()

cols=["per","surf_leak", "drain_flow", "deep_seep"]


for w in wtlds:
    cols.append("min_wtld_ddn_{0}".format(w))
    cols.append("mean_wtld_ddn_{0}".format(w))
    cols.append("max_wtld_ddn_{0}".format(w))
    
df = pd.DataFrame(columns=cols)

for n in range(mod.nper):
    df.loc[n]=[0]*len(cols)
    df.loc[n]["per"]=n
    df.loc[n]["surf_leak"] = mod.sum_surf_leak(n,n+1)
    df.loc[n]["drain_flow"]  = mod.sum_river_leak(n,n+1)
    df.loc[n]["deep_seep"]  = mod.sum_ghb_leak(n,n+1)
    dif=mod.elev[0]-h_drn[n]
    for w in wtlds:
        df.loc[n]["min_wtld_ddn_{0}".format(w)] = np.min(dif[np.where(wet_bdr_ras==w)])
        df.loc[n]["mean_wtld_ddn_{0}".format(w)] = np.mean(dif[np.where(wet_bdr_ras==w)])
        df.loc[n]["max_wtld_ddn_{0}".format(w)] = np.max(dif[np.where(wet_bdr_ras==w)])

sl = mod.get_surf_leak()
rl = mod.get_river_leak()
ss=sl[0][0]
rs=rl[0][0]

p = os.path.join(mod.path,"budgets")
for n in range(1,mod.nper):
    ss+=sl[n][0]
    rs+=rl[n][1]
    
mod.write_ascii(ss,p,"sleak")
mod.write_ascii(rs,p,"rleak")