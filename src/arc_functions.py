# -*- coding: utf-8 -*-
"""
@author: Jason.Roth
@title: State Water Management Engineer 
@affiliation: USDA-NRCS Minnesota
@email:jason.roth@mn.usda.gov
@repo:https://bitbucket.org/jason_roth/
Created on Fri Feb 02 15:14:04 2018
"""

import arcpy as ap
import os


def chk_file(f):
    if os.path.exists(f):
        ap.management.Delete(f)

def raster_to_ascii(in_ras, out_asc):
    chk_file(out_asc)
    ap.RasterToASCII_conversion(in_ras,out_asc)
    return out_asc
       
def get_extent(shp_path, xoff=0, yoff=0):
    """
    get the extent of a shape buffered out a distance in the x and y direction
    """
    ext = ap.Describe(shp_path).extent
    
    bnds = [ext.XMin-xoff, ext.XMax+xoff, ext.YMin-yoff, ext.YMax+yoff]
    
    bnds = [int(b) for b in bnds]
    
    return bnds

def row_col(extent, cs, x, y):
    """ extracts data from a grid at point x, y
        x, y are the coordinates at which to get the grid value
        grid is the data array to extract from
        grid_specs is the dictionary containing array bounds
    """
    xmin = extent[0]
    xmax = extent[1]
    ymin = extent[2]
    ymax = extent[3]
    
    if y >= ymin and y <= ymax and x >=xmin and x <= xmax: ## check to see if point is in the area of the grid
        ## calculate the cell location that the point falls into within the grid
        row = int((ymax- y)) / cs + 1 ## calculate row, note not offset by 1 bc np zero indexing
        col = int((x - xmin)) / cs + 1## calculate row, note not offset by 1 bc np zero indexing
        #print row, col
        return row, col 

def make_grid(grid_path, extent, cell_size):
    chk_file(grid_path)
    
    chk_file(grid_path.split('.')[0]+"_label."+grid_path.split('.')[1])
    
    origin_coord = "{0:0.3f} {1:0.3f}".format(extent[0],extent[2])
    
    yaxis_coord = "{0:0.3f} {1:0.3f}".format(extent[0], extent[3]+10)
    
    cs = int(cell_size)
    
    nrows = (extent[1]-extent[0])/cs
    
    ncols = (extent[3]-extent[2])/cs
    
    opposite_coord = "{0:0.3f} {1:0.3f}".format(extent[1],extent[3])
    
    ap.CreateFishnet_management(grid_path, origin_coord, yaxis_coord, cs, 
                                cs, ncols, nrows, opposite_coord, 'LABELS', 
                                '#', 'POLYGON')
    flds = ['ROW','COL']
    for f in flds:
        ap.AddField_management(grid_path, f, "SHORT")
    flds=["ROW", "COL","SHAPE@X", "SHAPE@Y"]
    with ap.da.UpdateCursor(grid_path,flds) as cursor:
        for row in cursor:  
            row[0], row[1] = row_col(extent, cs, row[2], row[3])    
            cursor.updateRow(row)
    del cursor
    
    return grid_path
 
def intersect_grid(in_shp, grd):
    
    tmp_shp = in_shp.split('.')[0]+"_tmp."+in_shp.split('.')[1]
    
    chk_file(tmp_shp)
    
    ap.Copy_management(in_shp, tmp_shp)
    
    chk_file(in_shp)
    
    ap.Intersect_analysis([tmp_shp, grd], in_shp, "ALL")
    
    chk_file(tmp_shp)
    
    ap.AddField_management(in_shp, "LENGTH", "FLOAT")
    
    ap.CalculateField_management(in_shp,"LENGTH",
                                 "!shape.length@meters!","PYTHON_9.3")
      
    return in_shp


def feature_to_raster(in_shp, in_fld, out_ras, cell_size, 
                              feat_type="POLYGON", ras_ext="", snap_ras=""):
    chk_file(out_ras)
    
    if snap_ras !=  "":
        ap.env.snapRaster = snap_ras
    else:
        ap.env.snapRaster = None
    
    if ras_ext != "":
        ap.env.extent = ap.Describe(ras_ext).extent
    else:
        ap.env.extent = None
        
    if feat_type == "POLYGON": 
        ap.PolygonToRaster_conversion(in_shp,in_fld,out_ras,cellsize=cell_size)
        
    elif feat_type == "POLYLINE":
        ap.PolylineToRaster_conversion(in_shp, in_fld, out_ras,
                                                       cellsize=cell_size)
    
    return out_ras

def feature_to_polyine(in_shp, out_shp):
    chk_file(out_shp)
    ap.FeatureToLine_management(in_shp, out_shp, attributes="ATTRIBUTES")
    return out_shp

def polygon_to_polyine(in_shp, out_shp, fld='ID'):
    chk_file(out_shp)
    
    ap.PolygonToLine_management(in_shp, out_shp)
    
    return out_shp


def resample_raster(bas_ras, out_ras, cs=30, typ="NEAREST", snp_ras=""):
    chk_file(out_ras)
    
    if snp_ras !=  "":
        snp_pth = os.path.join(snp_ras.path,snp_ras.name)
        ap.env.snapRaster = snp_pth
    else:
        ap.env.snapRaster = None
        
    out_ras = ap.Resample_management(bas_ras, out_ras, cs, typ)
    
    out_ras = ap.Raster(out_ras)
    
    return out_ras

def clip_shape(in_shp, clp_shp, out_shp):
    
    chk_file(out_shp)

    ap.Clip_analysis(in_shp, clp_shp, out_shp)
    
    return out_shp


def clip_raster_shape(bas_ras, clp_shp, out_ras):
    chk_file(out_ras)
    
    #bas_ras = ap.Raster(bas_ras)

    bas_ras = ap.sa.ExtractByMask(bas_ras, clp_shp)
    
    bas_ras.save(out_ras)
    
    out_ras = bas_ras
    
    return out_ras
    
def clip_raster_extent(bas_ras, extent, out_ras):
    chk_file(out_ras)

    clp_str="{0} {1} {2} {3}".format(extent[0],extent[2],extent[1],extent[3])
    ap.Clip_management(bas_ras, clp_str, out_ras)

    return out_ras

def attr_wtld_shp(shp, fld):
    ap.AddField_management(shp, fld, "SHORT")
    v=1
    with ap.da.UpdateCursor(shp,fld) as cursor:
        for row in cursor:  
            row[0]=v
            cursor.updateRow(row)
            v+=1
    return shp

def make_shapefile(shp_path, geo_typ="POLYGON", template =None, 
                           has_m="DISABLED", has_z="DISABLED", spa_ref=26915):
    """
    makes a shape file of a type of feature class
    """
    chk_file(shp_path)
     
    shp_path, name = os.path.split(shp_path)   
        
    spa_ref = ap.SpatialReference(spa_ref)
    
    ap.CreateFeatureclass_management(shp_path, name, geo_typ, 
                                     template, has_m, has_z, spa_ref)
    
    return os.path.join(shp_path, name)

    
def make_rectangle(shp_path, coords):
    """
    adds a rectangle to a polygon shapefile
    """
    cur = ap.da.InsertCursor(shp_path, ["SHAPE@"])
    arr = ap.Array()
    i = 0
    for yval in [2,3,3,2]:
        if i < 2:
            arr.add(ap.Point(coords[0], coords[yval]))
        else:
            arr.add(ap.Point(coords[1], coords[yval]))
        i+=1
    cur.insertRow([ap.Polygon(arr)])
    
    return shp_path

def make_buffer_rect(shp_path, extent=[0,1,0,1], spa_ref=26915):
    
    shp_path = make_shapefile(shp_path, spa_ref=spa_ref)

    shp_path = make_rectangle(shp_path, extent)
    
    return shp_path

def ras_to_pts(in_ras, out_pts, fld="VALUE"):
    ap.env.extent = None
    chk_file(out_pts)
    ap.RasterToPoint_conversion(in_ras, out_pts, fld)
    return out_pts
    
def krige(in_pts, out_ras, snap_ras=None, fld="GRID_CODE", cell_size=3):
    chk_file(out_ras)
    if snap_ras != None:
        ap.env.snapRaster = snap_ras
        snap_ras = ap.Raster(snap_ras)
        ap.env.extent = snap_ras.extent
    ap.Kriging_3d(in_pts, fld, out_ras, "Exponential", cell_size=cell_size)
    return out_ras