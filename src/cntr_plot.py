# -*- coding: utf-8 -*-
"""
@author: Jason.Roth
@title: State Water Management Engineer 
@affiliation: USDA-NRCS Minnesota
@email:jason.roth@mn.usda.gov
@repo:https://bitbucket.org/jason_roth/
Created on Tue Jan 09 12:54:44 2018
"""

import modflowModel as mfm
import numpy as np
import arcpy as ap
import os
import pandas as pd



mod_pth = ""

mod = mfm.modflowModel(mod_pth)

hds = mod.get_heads(0)

 