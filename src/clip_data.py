# -*- coding: utf-8 -*-
"""
@author: Jason.Roth
@title: State Water Management Engineer 
@affiliation: USDA-NRCS Minnesota
@email:jason.roth@mn.usda.gov
@repo:https://bitbucket.org/jason_roth/
Created on Fri Feb 02 11:20:54 2018

This code will clip geospatial and parameter data sets to be used for SWAMP

local data sets created:

buffered domain-polygon shp, 
grid-polygon shp, 
soils-polygon shp, 
soils-raster
surficial geology- polygon shp,
bedrock geology-polygon shp,
land surface elevation-ras, 
bedrock elevation-ras, 
wetlands-polygon shp,
wetlands boarder-polyline shp 
wetlands raster,
wetlands boarder raster

locol property tables:
soils
surf_geo
bdrk_geo
"""

import os
import pandas as pd
import arcpy as ap
import shutil as sh
import arc_functions as af

def get_fip_txt(cty_fip):
    """
    
    """
    if cty_fip/10.0<1.0:
        cty_fip_txt = '00{0}'.format(cty_fip)
    elif cty_fip/100.0<1.0:
        cty_fip_txt = '0{0}'.format(cty_fip)
    else:
        cty_fip_txt = '{0}'.format(cty_fip)
    return cty_fip_txt


def chk_dir(bas_dir, sub_dirs=["gis","data"]):
    for sd in sub_dirs:
        d = os.path.join(bas_dir, sd)
        if os.path.isdir(d):
            sh.rmtree(d)
            os.mkdir(d)

def clip_wtld(inshp, extshp, locshp, locbdr):
    af.chk_file(locshp)
    af.clip_shape(inshp,extshp,locshp)
    af.attr_wtld_shp(locshp,fld="val")
    af.chk_file(locbdr)
    af.polygon_to_polyine(locshp,locbdr)


def proc_bdrk(bdrk_ras, buf_shp, out_ras, snap_ras, cell_size=3):
    ##clip the bedrock 
    ## raster to points
    ## interpolate points
    ## clip new interp to domain
    bdrk = ap.Raster(bdrk_ras)
    #snap = ap.Raster(snap_ras)
    off = (bdrk.meanCellHeight+bdrk.meanCellWidth)/2
    #snap = ap.Raster(snap_rast)
    
    ext = af.get_extent(buf_shp, off, off)
    temp = os.path.split(snap_ras)[0]+"\\temp"
    #clp_ext=[ext.XMin-off,ext.YMin-off,ext.XMax+off,ext.YMax+off]
    temp = af.clip_raster_extent(bdrk, ext, temp)
    
    temp_pts = os.path.split(snap_ras)[0]+"\\temp_pts.shp"
    af.chk_file(temp_pts)
    af.chk_file(temp)
    temp_pts = af.ras_to_pts(bdrk,temp_pts)
    temp_ras = os.path.split(snap_ras)[0]+"\\temp_ras"
    af.chk_file(temp_ras)
    temp_ras=af.krige(temp_pts,temp_ras,snap_ras=snap_ras,cell_size=cell_size)
    af.chk_file(temp_pts) 
    bdrk_ras = ap.sa.ExtractByMask(temp_ras, snap_ras)
    af.chk_file(temp_ras)
    af.chk_file(out_ras)
    bdrk_ras.save(out_ras)

    return out_ras


def get_params(shp, df, fld="unit"):
    feats=[]
    ## get all units in the domain
    
    for row in ap.SearchCursor(shp):
        val = row.getValue(fld)
        if val not in feats:
            feats.append(val)
            
    idx = df.index
    rem = []
    for i in idx:
        if i not in feats:
            rem.append(i)
            
    df = df.drop(rem)
    return df
        
def attr_soils(shp, df, idx_col):
    """
    go through clipped soils shape, get unique values and assign an integer
    then attribute the df with the zone
    """
    ## add the new field
    ap.AddField_management(shp, "gridcode", "SHORT")

    flds=[idx_col,'gridcode']
    
    with ap.da.UpdateCursor(shp, flds) as cursor:
        
        for row in cursor:
            
            
            row[1]=df['gridcode'][row[0]][0]
            
            cursor.updateRow(row)
    return shp
def merge_fields(shp, fnam):
    
    ap.AddField_management(shp, fnam, "TEXT")
    
    ap.CalculateField_management(shp,fnam,"!AREASYMBOL!+!MUSYM!","PYTHON_9.3")    
        
    return shp
           
##############################################################################
## spa_ref (int) - ESPG code for the spatial reference of data sets, 
## default NAD83 UTM 15N = 26915

county = "Stearns"

spa_ref = 26915

bas_dir = "C:\\swamp"

name = "stears_sdi"

## buff (int) - buffer in model units to extend outward from extent shape
buff = 100

## model cell size either 1 or 3 m
cell_size = 3

## shapefile defining extent of model domain


           
##############################################################################
dirs = ['gis','dat']
# begin code execution
cwd = os.path.join(bas_dir,
                       "analyses\\{0}\\{1}".format(
                       county.lower().replace(" ","_"),name))

## check out the spatial extension
ap.CheckOutExtension('Spatial')

## set the workspace
ap.env.workspace = cwd

loc_wet_shp = '{0}\\gis\\wtld.shp'.format(cwd)

loc_wbdr_shp = '{0}\\gis\\wtld_bord.shp'.format(cwd)

loc_wet_ras = '{0}\\gis\\wtld'.format(cwd)

loc_sol_shp = '{0}\\gis\\soil.shp'.format(cwd)

loc_sol_ras = '{0}\\gis\\soil'.format(cwd)

loc_sol_dat = '{0}\\dat\\soil_props.csv'.format(cwd)

loc_geo_shp = '{0}\\gis\\surf.shp'.format(cwd)

loc_geo_ras = '{0}\\gis\\surf'.format(cwd)

loc_geo_dat = '{0}\\dat\\surf_props.csv'.format(cwd)

loc_brk_shp = '{0}\\gis\\bdrk.shp'.format(cwd)

loc_brk_ras = '{0}\\gis\\bdrk'.format(cwd)

loc_brk_dat = '{0}\\dat\\bdrk_props.csv'.format(cwd)

loc_brk_elv_ras = '{0}\\gis\\bdrk_elev'.format(cwd)

loc_img_ras = '{0}\\gis\\ortho'.format(cwd)

loc_elv_ras = '{0}\\gis\\elev'.format(cwd)

ext_ras = '{0}\\gis\\extent'.format(cwd)

grd_shp = '{0}\\gis\\grid.shp'.format(cwd)

ext_shp = "{0}\\extent.shp".format(cwd)

buf_shp = "{0}\\buffer.shp".format(cwd)

## cty_table (str) - lu table containing match for county fips and name.
cty_table = '{0}\\data\\county_table.csv'.format(bas_dir)

cty_txt = county.upper()

for d in dirs:
    nd = os.path.join(cwd,d)
    if not os.path.isdir(nd):
        os.mkdir(nd)

cty_dat_tab = pd.read_csv(cty_table, index_col='COUN_UC')

cty_fip = get_fip_txt(cty_dat_tab["FIPS_COUN"][cty_txt])

if os.path.exists(ext_shp) and cty_txt in cty_dat_tab.index:

    ## first clip the dem and use it as a mask raster
    buf_ext = af.get_extent(ext_shp,buff,buff)
    buf_shp = af.make_buffer_rect(buf_shp,buf_ext,spa_ref=spa_ref)
    elv_ras = 'F:\\Geodata\\elevation\\Lidar\\'+\
                '{0}\\elevation_data.gdb\\dem0{1}'.format(
                            cty_txt.lower().replace(" ",""),cell_size)      
    
    loc_elv_ras = af.clip_raster_extent(elv_ras, buf_ext, loc_elv_ras)
    
    ext_ras = af.feature_to_raster(ext_shp, "FID", ext_ras, cell_size, 
                              feat_type="POLYGON", ras_ext="", 
                              snap_ras=loc_elv_ras)
    
    #######################################################################
    ## clip wetland and make a polyline and raster for ea wetland
    #######################################################################

    wet_shp = 'F:\\Geodata\\wetlands\\NRCS\\'+\
                            'NRCS_CWD_a_mn{0}.shp'.format(cty_fip)

    loc_wet_shp = af.clip_shape(wet_shp,buf_shp,loc_wet_shp)
    
    
    #######################################################################
    ## clip the aerial imagery                  
    #######################################################################                                  
    img_ras =  'F:\\Geodata\\ortho_imagery\\2015\\27{0}\\'.format(cty_fip)+\
                'ortho_1-1_1n_s_mn{0}_2015_1.sid'.format(cty_fip)
                
    loc_img_ras = af.clip_raster_extent(img_ras, buf_ext, loc_img_ras)           

    #######################################################################
    ## clip and resample bdrk elev             
    #######################################################################                                  
    brk_elev = '{0}\\gis\\bdrk_elev_m'.format(bas_dir)
                
    loc_brk_elv_ras = proc_bdrk(brk_elev, buf_shp, 
                                loc_brk_elv_ras, loc_elv_ras, cell_size=3)      

    #######################################################################
    ## clip bdrk geo    
    #######################################################################      
    brk_shp = '{0}\\gis\\bdrk_geo.shp'.format(bas_dir)
    loc_brk_shp = af.clip_shape(brk_shp,buf_shp,loc_brk_shp)
    brk_dat = '{0}\\data\\bdrk_geo_lookup.csv'.format(bas_dir)
    
    df = pd.read_csv(brk_dat, index_col='gridcode')
    df = get_params(loc_brk_shp,df, fld='gridcode')
    df.to_csv(loc_brk_dat)
    loc_brk_ras = af.feature_to_raster(loc_brk_shp, 'gridcode', loc_brk_ras, 3, 
                               feat_type="POLYGON",ras_ext=loc_elv_ras,
                               snap_ras=loc_elv_ras)
    
    geo_shp = '{0}\\gis\\surf_geo.shp'.format(bas_dir)

    geo_dat = '{0}\\data\\surf_geo_lookup.csv'.format(bas_dir)       
        
    loc_geo_shp = af.clip_shape(geo_shp,buf_shp,loc_geo_shp)
    
    df = pd.read_csv(geo_dat, index_col='gridcode')
    
    df = get_params(loc_geo_shp,df, fld='gridcode')
    
    df.to_csv(loc_geo_dat) 
    
    loc_geo_ras = af.feature_to_raster(loc_geo_shp, 'gridcode', loc_geo_ras, 3, 
                       feat_type="POLYGON",ras_ext=loc_elv_ras,
                       snap_ras=loc_elv_ras)
        
    sol_shp = 'F:\\Geodata\\soils\\soil_mn{0}\\spatial\\'.format(cty_fip)+\
                              'soilmu_a_mn{0}.shp'.format(cty_fip)
    
    sol_dat = '{0}\\data\\soil_lookup.csv'.format(bas_dir)
    
    loc_sol_shp = af.clip_shape(sol_shp,buf_shp,loc_sol_shp)
    
    loc_sol_shp = merge_fields(loc_sol_shp, 'AREAMUSYM')
    
    df = pd.read_csv(sol_dat, index_col='areamusym')
    
    df = get_params(loc_sol_shp,df, fld='AREAMUSYM')
    
    loc_sol_shp = attr_soils(loc_sol_shp, df, idx_col='AREAMUSYM')
    
    df.to_csv(loc_sol_dat)
    
    loc_sol_ras = af.feature_to_raster(loc_sol_shp, 'gridcode', loc_sol_ras, 3, 
                   feat_type="POLYGON",ras_ext=loc_elv_ras,
                   snap_ras=loc_elv_ras)
    
else:
    print('County not found in county lookup table please check the name\n'
          +'and try again')