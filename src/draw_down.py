# -*- coding: utf-8 -*-
"""
@author: Jason.Roth
@title: State Water Management Engineer 
@affiliation: USDA-NRCS Minnesota
@email:jason.roth@mn.usda.gov
@repo:https://bitbucket.org/jason_roth/
Created on Wed Jan 10 13:56:32 2018
"""

import modflowModel as mfm
import numpy as np
import os
import shutil as sh
import arcpy as ap
import pandas as pd


sims=["base_tr_model", "drain_tr_model"]

mod_nam = "C:\\d_drive\\lateral_effects\\site_specific\\mcleod\\T10373\\{0}\\name.nam"

grd_pth = "C:\\d_drive\\lateral_effects\\site_specific\\mcleod\\T10373\\{0}\\ddns"

calculate_wetland_boundary_drawdown = True
## only needed if above is true
wetland_boundary_raster = "{0}\\{1}".format(os.getcwd(),"wet_bdr_ras")

if calculate_wetland_boundary_drawdown == True:
   ## import the raster
   wbr = ap.Raster(wetland_boundary_raster)
   wbr=ap.RasterToNumPyArray(wbr)
   wtlds = np.unique(wbr[np.where(wbr!=255]))
   per = 14
   df = pd.DataFrame(index=wtlds,columns=sims)                                                                     
                                                                        

## prepare output grids for each day of both simulations (no drains and drains)
for s in sims:                                                                
    mod = mfm.modflowModel(mod_nam.format(s))
    
    hds=mod.get_heads(0)
    gnd = mod.elev[0]
    fmt="{0:0.4f}"
    if not os.path.exists(grd_pth.format(s)):
        os.mkdir(grd_pth.format(s))
    
    for n in range(mod.nper+1):
        if n == 0:
            dif=np.where(hds[n]>0,gnd-mod.strt[0],mod.strt[0])
        else:
            dif=np.where(hds[n-1]>0,gnd-hds[n-1],hds[n-1])
            ## check if we are calculating drawdown at wetland boundary
            if calculate_wetland_boundary_drawdown == True and n == per:
                for w in wtlds:
                    df[s][w]=np.mean(dif[np.where(wbr==w)])
                    
                    
        nam = "ddn_day_{0}".format(n)
        mod.write_ascii(dif, grd_pth.format(s), nam, fmt)
        